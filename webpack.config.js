var path = require('path');

module.exports = {
  context: path.resolve('src'),
  entry: "./index.js",
  output: {
    path: path.resolve('build/js/'),
    publicPath: '/public/assets/js/',
    filename: "bundle.js"
  },
  devServer: {
    historyApiFallback: true,
    contentBase: 'public'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015']
        }
      }
    ]
},
  watch: true
};
