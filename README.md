# React Lunch N Learn Starter #

### What is this repository for? ###

* A starting point to get going with React. 
* This starter is using Webpack for bundling and ES6 transpilation
* Version 1.0.0

### How do I get set up? ###

* I assume you already have Node and Npm installed, if not: [get node](https://nodejs.org/en/)  
* git clone this repo
* cd /path/to/where/you/dowloaded/the/repo

```
#!bash

 npm install --save -g webpack webpack-dev-server 
 npm install
 npm start
```
*(may have to use sudo for global installs if you get permission errors)*


### Who do I talk to? ###

* Nick