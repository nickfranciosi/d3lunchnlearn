import d3 from 'd3';
import _ from 'lodash';

let dataset = _.map(_.range(100), () => Math.random() * 100);
let h = 300;
let w = 400;

let svg = d3.select('#chartArea').append('svg')
  .attr('width', w)
  .attr('height', h);

let xScale = d3.scale.ordinal()
    .domain(dataset) //what are the ranges of data
    .rangeBands([0 ,w], 0.2, 0.3);

let yScale = d3.scale.linear()
    .domain([0,d3.max(dataset) * 1.1])
    .range([0, h]);

let colorScale = d3.scale.linear()
    .domain([0, d3.max(dataset)])
    .range(['yellow', 'green']);

svg.selectAll('rect')
  .data(dataset)
  .enter()
  .append('rect')
  .attr('class', 'bar')
  .attr('x', xScale) //spacing of each
  .attr('y', (d) => h - yScale(d)) //position at bottom
  .attr('width', xScale.rangeBand())
  .attr('height', yScale)
  .attr('fill', colorScale)
